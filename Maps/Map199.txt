﻿
DisplayName "Tartarus (Northern Sabasa Continent)"



EVENT   1
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,427,13,1,0,0)
  0()



EVENT   2
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,2,148,238,8,0)
  0()



EVENT   3
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,2,148,238,8,0)
  0()



EVENT   4
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,2,148,238,8,0)
  0()



EVENT   5
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,2,148,238,8,0)
  0()



EVENT   6
 PAGE   1
  217()
  If(0,6,0)
   ShowMessageFace("sonia_fc2",2,0,2,1)
   ShowMessage("\\n<Sonya>なにこれ……ちょっと土が盛ってあるだけじゃない。 こんなの、強引に乗り越えて……")
   0()
  Else()
   ShowMessageFace("ruka_fc1",0,0,2,2)
   ShowMessage("\\n<Luka>なんだこれ……ちょっと土が盛ってあるだけじゃないか。 こんなの、強引に乗り越えて……")
   0()
  EndIf()
  ShowMessageFace("",0,0,2,3)
  ShowMessage("土がもこもこと盛り上がり、強引に行く手を阻んだ！")
  If(0,6,0)
   ShowMessageFace("sonia_fc3",2,0,2,4)
   ShowMessage("\\n<Sonya>動く土山にブロックされた！")
   0()
  Else()
   ShowMessageFace("ruka_fc1",0,0,2,5)
   ShowMessage("\\n<Luka>わぁっ、勝手に動いた！")
   0()
  EndIf()
  If(4,62,0)
   ShowMessageFace("gob_fc1",3,0,2,6)
   ShowMessage("\\n<Gob>じゃあ、ボクが…… わぁっ！　はじきかえされた！")
   0()
  EndIf()
  If(4,129,0)
   ShowMessageFace("brunhild_fc1",2,0,2,7)
   ShowMessage("\\n<Hild>ヒルデ、吶喊するよ…… ……ぎゃふん！　突破に失敗……")
   0()
  EndIf()
  If(1,1001,0,17,2)
   If(0,4,0)
    ShowMessageFace("alice_fc5",2,0,2,8)
    ShowMessage("\\n<Alice>なにやら、強力な魔術が込められているようだな。 強引に渡る事はできないだろう。")
    ShowMessageFace("alice_fc5",0,0,2,9)
    ShowMessage("\\n<Alice>しかし、土を操るノームならば…… これを乗り越える事も……")
    ShowMessageFace("alice_fc5",3,0,2,10)
    ShowMessage("\\n<Alice>……なにやら行動を誘導されている気がするな。 気に入らん……")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",2,0,2,11)
    ShowMessage("\\n<Ilias>強力な魔術が込められ、立ち入りを阻むようですね。 むりやり通ることは不可能でしょう。")
    ShowMessageFace("iriasu_fc4",2,0,2,12)
    ShowMessage("\\n<Ilias>しかし、土を操るノームならば…… この土山の解除も可能……")
    ShowMessageFace("iriasu_fc4",2,0,2,13)
    ShowMessage("\\n<Ilias>……なんだか行動を誘導されている気分です。 気に入りません……")
    0()
   EndIf()
   EndEventProcessing()
   0()
  EndIf()
  If(1,1001,0,18,0)
   If(0,4,0)
    ShowMessageFace("alice_fc5",0,0,2,14)
    ShowMessage("\\n<Alice>なにやら、強力な魔術が込められているようだな。 しかし、土を支配するノームの力なら……")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",0,0,2,15)
    ShowMessage("\\n<Ilias>強力な魔術が込められ、立ち入りを阻むようですね。 しかし、ノームを使えば……")
    0()
   EndIf()
   ShowMessageFace("gnome_fc1",0,0,2,16)
   ShowMessage("\\n<Gnome>……………………")
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0b,0x4d,0x61,0x67,0x69,0x63,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeSwitch(2100,2100,0)
   ShowMessageFace("",0,0,2,17)
   ShowMessage("ノームが指差すと、土山はたちまち崩れ去った！")
   If(4,12,0)
    ShowMessageFace("sylph_fc1",0,0,2,18)
    ShowMessage("\\n<Sylph>ノームちゃん、すご～い！")
    0()
   EndIf()
   If(0,4,0)
    ShowMessageFace("alice_fc5",0,0,2,19)
    ShowMessage("\\n<Alice>うむ、これでタルタロスに入れるな…… ……んっ？")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",0,0,2,20)
    ShowMessage("\\n<Ilias>これでタルタロスに入れますね…… ……んっ？")
    0()
   EndIf()
   205(-1,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x07,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x18,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
   241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0f,0x73,0x68,0x69,0x72,0x6f,0x75,0x73,0x61,0x67,0x69,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
   ChangeSwitch(2029,2029,0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x10,0x6d,0x6f,0x6e,0x5f,0x74,0x61,0x6d,0x61,0x6d,0x6f,0x32,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   205(10,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x07,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x13,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x07,0x69,0x00,0x69,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
   Wait(15)
   PictureDisplay(5,"80_sirousagi_st01",0,0,0,0,100,100,0,0)
   PictureMove(5,null,0,0,0,0,100,100,255,0,60,true)
   ShowMessageFace("sirousagi_fc1",0,0,2,21)
   ShowMessage("\\n<White Rabbit>ばばーん！")
   If(4,53,0)
    ShowMessageFace("slime_fc1",2,0,2,22)
    ShowMessage("\\n<Lime>わぁっ！　またウサギだ！")
    0()
   EndIf()
   If(0,4,0)
    ShowMessageFace("alice_fc5",2,0,2,23)
    ShowMessage("\\n<Alice>白兎、また出たか！ 今度はいったい何のつもりだ！？")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",7,0,2,24)
    ShowMessage("\\n<Ilias>また出ましたね、白兎！ これはいったい何の嫌がらせなのです！？")
    0()
   EndIf()
   ShowMessageFace("sirousagi_fc1",0,0,2,25)
   ShowMessage("\\n<White Rabbit>イヤガラセじゃないよ～！ 鎌もった厨二病に追い掛けられながら、頑張って造ったのに。")
   ShowMessageFace("sirousagi_fc1",0,0,2,26)
   ShowMessage("\\n<White Rabbit>あのヒト、どうにかならないのかな？ まったく、親の顔が見たいよね……")
   If(0,4,0)
    ShowMessageFace("alice_fc5",2,0,2,27)
    ShowMessage("\\n<Alice>ええい、御託はいい！ これ以上邪魔をする気なら、ただじゃおかんぞ！")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",7,0,2,28)
    ShowMessage("\\n<Ilias>戯れ言は不要です！ これ以上邪魔をする気なら、ここで滅ぼしますよ！")
    0()
   EndIf()
   ShowMessageFace("sirousagi_fc1",0,0,2,29)
   ShowMessage("\\n<White Rabbit>ボク、邪魔なんてしてないよ。 この先に進む前に、ノームくらいは持ってほしかっただけ。")
   ShowMessageFace("sirousagi_fc1",0,0,2,30)
   ShowMessage("\\n<White Rabbit>さあ、見てきなよ…… 混沌に覆われた、この先の世界をさ。")
   ShowMessageFace("sirousagi_fc1",0,0,2,31)
   ShowMessage("\\n<White Rabbit>それじゃあね～！")
   PictureClear(5)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x10,0x6d,0x6f,0x6e,0x5f,0x74,0x61,0x6d,0x61,0x6d,0x6f,0x32,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   205(10,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x07,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x13,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x07,0x69,0x00,0x69,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
   ChangeSwitch(2029,2029,1)
   241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x6f,0x6f,0x61,0x6e,0x61,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
   If(0,4,0)
    ShowMessageFace("alice_fc5",6,0,2,32)
    ShowMessage("\\n<Alice>あっ、待て…… ……くっ、また逃げられたか。")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",7,0,2,33)
    ShowMessage("\\n<Ilias>あっ、待ちなさい…… ……おのれ、また逃げられましたか。")
    0()
   EndIf()
   If(4,531,0)
    ShowMessageFace("saniria_fc2",0,0,2,34)
    ShowMessage("\\n<King of San Ilia>あれが例のウサギか…… なんとも不可解な存在だな。")
    0()
   EndIf()
   If(4,129,0)
    ShowMessageFace("brunhild_fc1",0,0,2,35)
    ShowMessage("\\n<Hild>エネルギー、測定不可能だよ…… なんでか虚数が出るの……")
    0()
   EndIf()
   If(0,6,0)
    ShowMessageFace("sonia_fc2",2,0,2,36)
    ShowMessage("\\n<Sonya>この先、何があるんだろう…… ずいぶん、もったいぶった事を言ってたよね。")
    0()
   Else()
    ShowMessageFace("ruka_fc1",0,0,2,37)
    ShowMessage("\\n<Luka>この先、何があるんだろう…… ずいぶん、もったいぶった事を言ってたよね。")
    0()
   EndIf()
   If(0,4,0)
    ShowMessageFace("alice_fc5",2,0,2,38)
    ShowMessage("\\n<Alice>ふん、いつもそれっぽい事ばかり言いおって！ どうせ――")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",7,0,2,39)
    ShowMessage("\\n<Ilias>まったく、毎回曖昧な事ばかり……！ おおかた――")
    0()
   EndIf()
   241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0f,0x73,0x68,0x69,0x72,0x6f,0x75,0x73,0x61,0x67,0x69,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
   ChangeSwitch(2029,2029,0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x10,0x6d,0x6f,0x6e,0x5f,0x74,0x61,0x6d,0x61,0x6d,0x6f,0x32,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   205(10,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x07,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x13,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x07,0x69,0x00,0x69,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
   PictureDisplay(5,"80_sirousagi_st01",0,0,0,0,100,100,0,0)
   PictureMove(5,null,0,0,0,0,100,100,255,0,60,true)
   ShowMessageFace("sirousagi_fc1",0,0,2,40)
   ShowMessage("\\n<White Rabbit>あっ、そうだ！　忘れてた！")
   If(0,6,0)
    ShowMessageFace("sonia_fc3",2,0,2,41)
    ShowMessage("\\n<Sonya>また出た！")
    0()
   Else()
    ShowMessageFace("ruka_fc1",0,0,2,42)
    ShowMessage("\\n<Luka>わぁっ、また出た！")
    0()
   EndIf()
   ShowMessageFace("sirousagi_fc1",0,0,2,43)
   ShowMessage("\\n<White Rabbit>はい、これあげる！ 何の役にも立たないけど、邪魔だからあげる！")
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventory_Item(522,0,0,1)
   ShowMessageFace("sirousagi_fc1",0,0,2,44)
   ShowMessage("\\n<White Rabbit>どつきあってる時に、奪ってやったの。 メガネ、メガネって言って、傑作だったよ！")
   If(0,6,0)
    ShowMessageFace("sonia_fc3",2,0,2,45)
    ShowMessage("\\n<Sonya>あのクール（風）キャラにそんな事言わせたの！？")
    0()
   Else()
    ShowMessageFace("ruka_fc1",0,0,2,46)
    ShowMessage("\\n<Luka>ネロさん、そんな事言ったんだ……")
    0()
   EndIf()
   ShowMessageFace("sirousagi_fc1",0,0,2,47)
   ShowMessage("\\n<White Rabbit>実は嘘、言ってないよ。 それどころか、お返しとばかりに頭をもぎ取られちゃった……")
   ShowMessageFace("sirousagi_fc1",0,0,2,48)
   ShowMessage("\\n<White Rabbit>見ての通り、すぐ生えてきたから心配ご無用。 そういうわけで、また厨二が襲ってくるからボク行くね。")
   PictureClear(5)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x10,0x6d,0x6f,0x6e,0x5f,0x74,0x61,0x6d,0x61,0x6d,0x6f,0x32,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   205(10,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x07,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x13,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x07,0x69,0x00,0x69,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
   ChangeSwitch(2029,2029,1)
   241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x6f,0x6f,0x61,0x6e,0x61,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
   If(0,4,0)
    ShowMessageFace("alice_fc5",3,0,2,49)
    ShowMessage("\\n<Alice>いったい、何なのだ……")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",2,0,2,50)
    ShowMessage("\\n<Ilias>いったい、何なのですか……")
    0()
   EndIf()
   If(4,79,0)
    ShowMessageFace("nuruko_fc1",2,0,2,51)
    ShowMessage("\\n<Nuruko>きゅ……")
    0()
   EndIf()
   205(-1,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x07,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x15,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
   ShowMessageFace("ruka_fc1",0,0,2,52)
   ShowMessage("\\n<Luka>気を取り直して、タルタロスに入ろう！")
   ShowMessageFace("",0,0,2,53)
   ShowMessage("この先に、何があるのか。 そして、どんな異世界に繋がっているのか――")
   ShowMessageFace("",0,0,2,54)
   ShowMessage("それを、自分の目で確かめなければならないのだ。")
   0()
  EndIf()
  0()
 PAGE   2
  // condition: switch 2100 is ON
  0()



EVENT   7
 PAGE   1
  If(0,2100,1)
   ShowMessageFace(binary"",0,0,2,1)
   ShowMessage("\\n<Student>変なウサギが、入り口に土を積んで山を造ったんです！ おかげで、タルタロスに入れなくなってしまいました……")
   ShowMessageFace(binary"",0,0,2,2)
   ShowMessage("\\n<Student>どうやって土を積んだのかって？ スコップで……鼻歌を歌いながら……")
   If(0,4,0)
    ShowMessageFace("alice_fc5",3,0,2,3)
    ShowMessage("\\n<Alice>白兎め……またもや、意味の分からん事を……")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",2,0,2,4)
    ShowMessage("\\n<Ilias>白兎……今度は何を企んでいるのでしょう……")
    0()
   EndIf()
   0()
  Else()
   ShowMessageFace("",0,0,2,5)
   ShowMessage("\\n<Student>土山が除去され、調査が再開できるようになりました。 護衛騎士も少ない今、あまり深くは潜れませんが……")
   0()
  EndIf()
  0()



EVENT   8
 PAGE   1
  If(0,2100,1)
   ShowMessageFace("",0,0,2,1)
   ShowMessage("\\n<Scholar>なんということだ、これでは調査が進まないではないか……")
   0()
  Else()
   ShowMessageFace("",0,0,2,2)
   ShowMessage("\\n<Scholar>調査はじっくり進めるもの。 相棒の気持ちも分かるが、焦っては成果が出ないからな。")
   355("actor_label_jump")
   EndEventProcessing()
   DefineLabel("41")
   ShowMessageFace("stein_fc2",0,0,2,3)
   ShowMessage("\\n<Promestein>その通りですね。 焦って事を為そうとすれば、見逃しが生じます……")
   EndEventProcessing()
   0()
  EndIf()
  0()



EVENT   9
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<Scholar>本国からの帰還命令なんて、知ったことか！ これは、前王の弔い合戦なんだ！")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<Scholar>このタルタロスを調査し、全貌を解き明かす…… それが、視察中に亡くなられた前王に対するはなむけなんだよ！")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("529")
  ShowMessageFace("sara_fc1",4,0,2,3)
  ShowMessage("\\n<Sara>無理をする必要はないわ。 前王の死は、あなた達の責任じゃないから……")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("\\n<Scholar>女王陛下……いや、サラ王女様。 しかし私は、役目も半ばでおめおめ帰国する事はできません。")
  EndEventProcessing()
  0()



EVENT   10
 PAGE   1
  // condition: switch 2029 is ON
  0()



EVENT   11
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeSelfSwitch("A",0)
  ChangeInventory_Item(8,0,0,1)
  0()
 PAGE   2
  // condition: self-switch A is ON
  0()



EVENT   12
 PAGE   1
  // condition: variable 1028 >= 8
  // condition: party contains actor #529
  ChangeSelfSwitch("A",0)
  ShowMessageFace("sara_fc1",4,0,2,1)
  ShowMessage("\\n<Sara>お父様……")
  If(0,6,0)
   ShowMessageFace("sonia_fc2",2,0,2,2)
   ShowMessage("\\n<Sonya>あっ、そうか…… サバサ前王は、ここで……")
   ShowMessageFace("sonia_fc2",2,0,2,3)
   ShowMessage("\\n<Sonya>大丈夫、サラ？ 辛いのなら、ポケット魔王城で待機していた方が……")
   0()
  Else()
   ShowMessageFace("ruka_fc1",0,0,2,4)
   ShowMessage("\\n<Luka>……サラ、大丈夫？ 辛いなら、ポケット魔王城で待機した方が……")
   0()
  EndIf()
  ShowMessageFace("sara_fc1",1,0,2,5)
  ShowMessage("\\n<Sara>いえ、行かせて。 お父様が亡くなられた場所、いつか訪れたかったの。")
  ShowMessageFace("sara_fc1",4,0,2,6)
  ShowMessage("\\n<Sara>それに、現実から眼を逸らしてるだけだろうけど…… 今も父様は、どこかで……")
  If(0,4,0)
   ShowMessageFace("alice_fc5",3,0,2,7)
   ShowMessage("\\n<Alice>余の母は、魔の大陸のタルタロス付近で失踪した。 長らく死んだと思われていたが、つい近年姿を現した。")
   ShowMessageFace("alice_fc5",0,0,2,8)
   ShowMessage("\\n<Alice>決して期待を持たせるわけではないが…… タルタロスでは、何があっても不思議ではあるまい。")
   0()
  EndIf()
  ShowMessageFace("",0,0,2,9)
  ShowMessage("サバサ王は、視察中に大穴に落下。 死体は発見されていない……")
  ShowMessageFace("",0,0,2,10)
  ShowMessage("あまりにも不自然な状況である事は否定できない。 巷に流布する生存説も、聞き流せない信憑性があった。")
  ShowMessageFace("",0,0,2,11)
  ShowMessage("そして、僕達は知っているのだ。 タルタロスが異世界へのトンネルである事を――")
  ShowMessageFace("sara_fc1",1,0,2,12)
  ShowMessage("\\n<Sara>ごめんなさい、気遣いは無用よ。 さあ、行きましょう！")
  0()
 PAGE   2
  // condition: self-switch A is ON
  0()
